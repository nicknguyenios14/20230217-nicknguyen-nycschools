//
//  SceneDelegate.swift
//  NYCSchools
//
//  Created by Thinh Nguyen on 2/17/23.
//

import UIKit

final class SceneDelegate: UIResponder, UIWindowSceneDelegate {
  var window: UIWindow?
  
  private let schoolTableViewController: UINavigationController = {
    let schoolTableViewController = SchoolsTableViewController()
    
    schoolTableViewController.title = "NYC Schools"
    return UINavigationController(rootViewController: schoolTableViewController)
  }()
  
  func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
    guard let windowScene = (scene as? UIWindowScene) else { return }
    window = UIWindow(windowScene: windowScene)
    let viewController = schoolTableViewController
    window?.rootViewController = viewController
    window?.makeKeyAndVisible()
  }
}
