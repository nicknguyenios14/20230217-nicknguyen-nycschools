//
//  SchoolDetail.swift
//  NYCSchools
//
//  Created by Thinh Nguyen on 2/17/23.
//

import Foundation

struct SchoolDetail: Codable {
  let satTestTakers: String
  let satReadingScore: String
  let satMathScore: String
  let satWritingScore: String
  
  enum CodingKeys: String, CodingKey {
    case satTestTakers = "num_of_sat_test_takers"
    case satReadingScore = "sat_critical_reading_avg_score"
    case satMathScore = "sat_math_avg_score"
    case satWritingScore = "sat_writing_avg_score"
  }
}
