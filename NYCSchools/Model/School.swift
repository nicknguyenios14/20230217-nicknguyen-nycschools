//
//  School.swift
//  NYCSchools
//
//  Created by Thinh Nguyen on 2/17/23.
//

import Foundation

struct School: Codable {
  let dbn: String
  let schoolName: String
  
  enum CodingKeys: String, CodingKey {
    case dbn
    case schoolName = "school_name"
  }
}
