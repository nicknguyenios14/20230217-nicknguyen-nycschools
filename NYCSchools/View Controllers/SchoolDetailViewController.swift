//
//  SchoolDetailViewController.swift
//  NYCSchools
//
//  Created by Thinh Nguyen on 2/17/23.
//

import UIKit

class SchoolDetailViewController: UITableViewController {
  
  var school: School
  var schoolDetails: [SchoolDetail] = []
  
  init(school: School) {
    self.school = school
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .white
    navigationItem.title = school.schoolName
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifier.schoolDetailCell)
    getSchoolDetail()
  }
  
  private func getSchoolDetail() {
    NetworkService.shared.fetchSchoolDetail(dbn: school.dbn) { [weak self] result in
      switch result {
      case .success(let schoolDetails):
        self?.schoolDetails = schoolDetails
        self?.tableView.reloadData()
      case .failure(let error):
        print(error)
      }
    }
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return schoolDetails.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.schoolDetailCell, for: indexPath)
    let schoolDetail = schoolDetails[indexPath.row]
    
    cell.textLabel?.text = "READING: \(schoolDetail.satReadingScore) MATH: \(schoolDetail.satMathScore) WRITING: \(schoolDetail.satWritingScore)"
    return cell
  }
}
