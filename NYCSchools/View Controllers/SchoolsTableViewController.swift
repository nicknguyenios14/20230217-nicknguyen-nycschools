//
//  ViewController.swift
//  NYCSchools
//
//  Created by Thinh Nguyen on 2/17/23.
//

import UIKit

class SchoolsTableViewController: UITableViewController {
  // Propeties
  private var schools: [School] = [] {
    didSet {
      schools.sort { $0.schoolName < $1.schoolName }
    }
  }
  
  // View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifier.schoolCell)
    downloadSchools()
  }
  
  // Action
  private func downloadSchools() {
    NetworkService.shared.fetchSchools { [weak self] result in
      switch result {
      case .success(let schools):
        self?.schools = schools
        self?.tableView.reloadData()
      case .failure(let error):
        // handle error, show alert
        print(error)
      }
    }
  }
  
  // Table View DataSource & Delegate
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return schools.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let schoolTableViewCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.schoolCell, for: indexPath)
    let schoolName = schools[indexPath.row].schoolName
    schoolTableViewCell.textLabel?.text = schoolName
    schoolTableViewCell.textLabel?.numberOfLines = 0
    schoolTableViewCell.accessoryType = .disclosureIndicator
    return schoolTableViewCell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    let selectedSchool = schools[indexPath.row]
    let schoolDetailViewController = SchoolDetailViewController(school: selectedSchool)
    show(schoolDetailViewController, sender: indexPath)
  }
}

