//
//  NetworkService.swift
//  NYCSchools
//
//  Created by Thinh Nguyen on 2/17/23.
//

import Foundation

final class NetworkService {
  
  // MARK: - Singleton
  
  static let shared = NetworkService()
  
  // MARK: - Action
  
  func fetchSchools(completion: @escaping (Result<[School], NetworkError>) -> Void) {
    guard let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else {
      return
    }
    let urlRequest = URLRequest(url: url)
    
    URLSession.shared.dataTask(with: urlRequest) { data, response, error in
      if let error = error {
        NSLog(error.localizedDescription)
        completion(.failure(.badURL))
        return
      }
      
      guard let response = response as? HTTPURLResponse, response.isOK else {
        completion(.failure(.badResponse))
        return
      }
      
      guard let data = data else {
        completion(.failure(.unknown))
        return
      }
      
      do {
        let schools = try JSONDecoder().decode([School].self, from: data)
        DispatchQueue.main.async {
          completion(.success(schools))
        }
      } catch {
        completion(.failure(.decodeError))
        return
      }
    }.resume()
  }
  
  func fetchSchoolDetail(dbn: String, completion: @escaping (Result<[SchoolDetail], NetworkError>) -> Void) {
    guard let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)") else {
      return
    }
    let urlRequest = URLRequest(url: url)
    
    URLSession.shared.dataTask(with: urlRequest) { data, response, error in
      if let error = error {
        NSLog(error.localizedDescription)
        completion(.failure(.badURL))
        return
      }
      
      guard let response = response as? HTTPURLResponse, response.isOK else {
        completion(.failure(.badResponse))
        return
      }
      
      guard let data = data else {
        completion(.failure(.unknown))
        return
      }
      
      do {
        let schoolDetail = try JSONDecoder().decode([SchoolDetail].self, from: data)
        DispatchQueue.main.async {
          completion(.success(schoolDetail))
        }
      } catch {
        completion(.failure(.decodeError))
        return
      }
    }.resume()
  }
}
