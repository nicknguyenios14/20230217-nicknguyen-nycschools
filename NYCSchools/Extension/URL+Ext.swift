import UIKit

extension HTTPURLResponse {
  var isOK: Bool {
    (200...299).contains(statusCode)
  }
}

