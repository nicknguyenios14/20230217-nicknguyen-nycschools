//
//  CellIdentifier.swift
//  NYCSchools
//
//  Created by Thinh Nguyen on 2/17/23.
//

import UIKit

struct CellIdentifier {
  static let schoolCell = "SchoolCell"
  static let schoolDetailCell = "SchoolDetailCell"
}
